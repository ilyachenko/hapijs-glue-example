# README

Using glue to compose a hapi server.

# Installation

Prerequisites: Node >= 4.x

```bash
npm install
```

Optional: nodemon

```bash
npm install -g nodemon
```

# Start server

```bash
node .

# or nodemon
nodemon .
```

# Changelog

* v0.0.5: added `lout`, `ms`, `image-type`
* v0.0.4: added `cryptiles`, `hoek`, `mkdirp`, `joi`
* v0.0.3: added `bassmaster` for handling combined requests, `blipp` see the table of routes during dev
* v0.0.2: added `nes` websocket.

# License

* MIT
