const ms = require('ms');

module.exports = function date(timestamp = 0) {
	return ms(+timestamp, { long: true });
};