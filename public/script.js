var Nes = require('nes/client');
var mySocket = new Nes.Client('ws://localhost:9000');

var btn = document.querySelector('.btn');
var btn1 = document.querySelector('#cms');
var timespan = document.querySelector('#time');

if (btn) {
	btn.addEventListener('click', function (e) {
		console.log(e);
		alert('You clicked me');
	});
}


mySocket.connect(function (err) {
	// ping
	mySocket.request('hello', function (err, payload) {
		console.log('request `hello`: ' + JSON.stringify(payload));
	});

	mySocket.subscribe('/item/5',
		function (update, flags) {
			console.log('subscribe /item/5: ');
			console.log(update, flags);
		}, function (err) { });

	mySocket.subscribe('/time',
		function (update, flags) {
			timespan.innerHTML = Date(update.message);
			console.log('subscribe /time: %s, \n%s', JSON.stringify(update), JSON.stringify(flags));
		}, function (err) { });

	mySocket.onUpdate = function (update) {
		console.log('on update: ' + update);
	}
	// btn click
	btn1.addEventListener('click', function (e) {
		var data = {
			id: 'hello',
			msg: 'from client: hello server'
		};
		mySocket.message(
			data,
			function (err, msg) {
				console.log(err, msg);
			});
	})
});
