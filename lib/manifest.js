module.exports = {
	server: {
		cache: [{
			name: 'redisCache',
			engine: 'catbox-redis',
			host: process.env.REDIS_HOST || '192.168.0.105', // redis server IP
			password: process.env.REDIS_PASS || 'YOLO', // redis server AUTH password
			database: process.env.REDIS_DB || '1', // redis DB
			partition: 'cache'
		}]
	},
	connections: [{
		labels: ['api'],
		host: 'localhost',
		port: 9000
	}],
	registrations: [
		{ plugin: 'inert' },
		{ plugin: 'vision' },
		{ plugin: 'lout' },
		{ plugin: 'bassmaster' },
		{ plugin: './routes/home' },
		{ plugin: './routes/cache' },
		{ plugin: './routes/upload' },
		{
			plugin: './routes/prefixedApiRoutes', // localhost:9000/v2/api/ping
			options: {
				routes: {
					prefix: '/v2'
				}
			}
		},
		{ plugin: './routes/static' },
		{ plugin: './routes/404' }
	]
};
