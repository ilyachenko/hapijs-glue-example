const Glue = require('glue');
const Path = require('path');

const manifest = require('./lib/manifest');
const options = {
	relativeTo: __dirname
};

if (process.env.NODE_ENV !== 'production') {
	manifest.registrations.push({
		plugin: 'blipp'
	});
}

Glue.compose(manifest, options, (err, server) => {
	if (err) {
		console.warn(err);
		throw err;
	}

	server.views({
		defaultExtension: 'hbs',
		isCached: false,
		engines: {
			hbs: require('handlebars')
		},
		relativeTo: __dirname,
		layout: 'default',
		path: './templates',
		helpersPath: './templates/helpers',
		layoutPath: './templates/layout',
		partialsPath: './templates/partials'
	});

	server.start((err) => {
		if (err) {
			throw err;
		}
		console.log('HAPI is running at ' + server.info.uri);
	});
});
