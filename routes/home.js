exports.register = (server, options, next) => {
	server.route([
		{
			method: 'GET',
			path: '/',
			handler: function (request, reply) {
				var data = {
					title: 'Home Page',
					user: {
						firstname: 'John',
						lastname: 'Doe',
						age: 30
					}
				};
				reply.view('index', data);
			}
		}, {
			method: 'GET',
			path: '/about',
			handler: function (request, reply) {
				reply.view('about', {
					title: 'About Us',
					terms: 'Fantastic terms... blah blah blah'
				});
			}
		},
	]);

	return next();
};

exports.register.attributes = {
	name: 'home route',
	version: '1.0.1'
};
