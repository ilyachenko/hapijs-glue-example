const faker = require('faker');
const Joi = require('joi');

exports.register = (server, options, next) => {
	function onMessage(socket, data, next) {
		console.log(data);
		next('ok')
	}
	// register nes
	server.register({
		register: require('nes'),
		options: {
			onMessage: onMessage
		}
	});
	// nes
	server.broadcast('welcome!');
	server.subscription('/item/{id}');
	server.subscription('/time');

	server.route([
		{
			method: 'GET',
			path: '/api/ping',
			handler: function (request, reply) {
				reply({
					status: 'ok'
				});
			}
		}, {
			method: 'GET',
			path: '/api/user/{id}',
			config: {
				validate: {
					params: {
						id: Joi.string().alphanum().max(50).required()
					}
				},
				tags: ['api', 'fake'],
				handler: function (request, reply) {
					reply({
						userId: String(request.params.id),
						displayName: faker.name.findName(),
						email: faker.internet.email(),
						avatar: faker.internet.avatar()
					});
				}
			}

		}, {
			method: 'GET',
			path: '/realtime',
			handler: function (request, reply) {
				// publish over time
				request.socket.publish('/time', {
					id: 90,
					message: (new Date).getTime()
				});
				request.socket.publish('/item/5', {
					id: 555,
					message: 'this is item #5'
				});
				return reply();
			},
			config: {
				id: 'hello'
			}
		}
	]);

	return next();
};

exports.register.attributes = {
	name: 'prefixed API routes',
	version: '1.0.0'
};
