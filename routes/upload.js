const fs = require('fs');
const path = require('path');
const mkdirp = require('mkdirp');
const Hoek = require('hoek');
const cryptiles = require('cryptiles');
const imageType = require('image-type');

exports.register = (server, opt, next) => {
	server.method('Upload.isImage', (request = {}, reply) => {
		// Support non-svg images
		let isImage = imageType(request._data);
		return reply(isImage);
	});

	server.route([
		{
			method: 'GET',
			path: '/upload',
			handler: (request, reply) => {
				reply.view('upload');
			}
		},
		{
			method: 'POST',
			path: '/upload',
			config: {
				payload: {
					output: 'stream',
					allow: 'multipart/form-data',
					parse: true,
					maxBytes: 1048576, // 1MB
					timeout: 5 * 1000 // 5s
				},
				description: 'Upload files',
				tags: ['Image'],
				pre: [
					{ method: 'Upload.isImage(payload.file)', assign: 'isImage' }
				],
				handler: function (request, reply) {
					// upload images only
					if (!request.pre.isImage) {
						return reply.view('upload', {
							success: false,
							error: true,
							errorMsg: '415 Unsupported Media Type'
						}).code(415);
					}

					const CURRENT_YEAR = new Date().getFullYear();
					const CURRENT_MONTH = new Date().getMonth() + 1;
					const UPLOAD_PATH = `public/uploads/${CURRENT_YEAR}/${CURRENT_MONTH}`;
					const FILE_EXT = request.pre.isImage.ext;
					const RANDOM_FILE_NAME = cryptiles.randomString(12);
					const newFileName = `${RANDOM_FILE_NAME}.${FILE_EXT}`;

					mkdirp(path.join(__dirname, '..', UPLOAD_PATH), (err) => {
						Hoek.assert(!err, err);

						let image = fs.createWriteStream(path.join(__dirname, '..', UPLOAD_PATH, newFileName));
						request.payload.file.pipe(image);

						image.on('finish', function () {
							reply.view('upload', {
								success: true,
								raw: {
									publicURL: `/assets/uploads/${CURRENT_YEAR}/${CURRENT_MONTH}/${newFileName}`,
									internal: {
										filePath: `${UPLOAD_PATH}/${newFileName}`,
										imageType: request.pre.isImage
									}
								}
							});
						});
					});
				}
			}
		}
	]);



	next();
};

exports.register.attributes = {
	name: 'upload route',
	version: '1.0.1'
}