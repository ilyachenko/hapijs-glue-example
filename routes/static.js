const Path = require('path');

exports.register = (server, options, next) => {
	server.route([{
		method: 'GET',
		path: '/assets/{p*}',
		config: {
			description: 'Catch all route',
			handler: {
				directory: {
					path: Path.join(__dirname, '../public'),
					listing: true
				}
			}
		}
	}]);

	next();
};

exports.register.attributes = {
	name: 'static route',
	version: '1.0.0'
};
