exports.register = (server, options, next) => {
	server.route([{
		method: 'GET',
		path: '/{p*}',
		handler: function(request, reply) {
			reply.view('404',
				null, // no data
				{
					layout: false // no layout
				});
		}
	}]);

	next();
};

exports.register.attributes = {
	name: 'bad route',
	version: '1.0.0'
};
