const Hoek = require('hoek');
const faker = require('faker');
const Joi = require('joi');
const ms = require('ms');

exports.register = (server, opt, next) => {
	server.route([
		{
			method: 'GET',
			path: '/cache/{id?}',
			config: {
				validate: {
					params: {
						id: Joi.string().optional()
					},
					query: false
				},
				tags: ['cache'],
				description: 'Route cached with catbox-redis',
				handler: function (request, reply) {
					const userId = request.params.id ? encodeURIComponent(request.params.id) : 9000;
					request.server.methods.User.getById(userId, (err, result, cached, report) => {
						Hoek.assert(!err, err);

						reply.view('cache', {
							user: result,
							cached: cached,
							report: report
						}, { layout: false });
					})
				}
			}
		}
	]);

	server.method('User.getById', (uid, next) => {
		// simulate a long operation
		setTimeout(() => {
			return next(null, {
				uid: uid,
				email: faker.internet.email(),
				displayName: faker.name.findName()
			});
		}, 1500);

	}, {
			cache: {
				cache: 'redisCache',
				expiresIn: ms('60s'),
				generateTimeout: ms('2s'),
				staleIn: ms('50s'),
				staleTimeout: ms('2s')
			}
		});
	next();
};

exports.register.attributes = {
	name: 'catbox cached route',
	version: '1.0.2'
}